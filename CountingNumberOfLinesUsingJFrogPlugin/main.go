package main

import (
	"github.com/jfrog/jfrog-cli-core/v2/plugins"
	"github.com/jfrog/jfrog-cli-core/v2/plugins/components"
	"github.com/jfrog/jfrog-cli-plugin-template/commands"
)

func main() {
	plugins.PluginMain(getApp())
	//fmt.Println("Hello, Welcome to Arham's World!")
}

func getApp() components.App {
	app := components.App{}
	app.Name = "counting-lines"
	app.Description = "It will count the number of lines of each file present in a repo."
	app.Version = "v0.1.2"
	app.Commands = getCommands()
	return app
}

func getCommands() []components.Command {
	return []components.Command{
		commands.GetCountingCommands()}
}
